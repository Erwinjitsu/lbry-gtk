# Rules of Contributing:

- Use PascalCase.
- Do not use shortened variable/function names.
- Use [Glade](https://glade.gnome.org/) for designing the GUI.
- When working on the GUI, use the Glade files as much as possible.
- When creating containers, use GtkBox instead of GtkGrid as much as possible.
- Run your code through black formatter, before commit. (black -l 80 ./)
- When all work is done, squash your commits, before merge.
- Use the listed docs.

## C Specific:

Rules for files:
- Global names have to start with the file name (variables, functions, structs, etc.).
- Use single line comments: `// The comment you made..`
- Header files go to [header directory](share/lbry-gtk/CSource/Header).
- Line length at maximum is 80 characters, with tabs taking 4 spaces.
- Always format the code for pull request with the command specified below.

For instructions, check out the [Makefile](share/lbry-gtk/CSource/Makefile)

In short, all the commands you need are:
- Build the project with `make`
- Test the functionality with `make test`
- Format code before commit with `make format`
- Clean up all object files with `make clean`

Programs needed for formatting:
- [Black](https://github.com/psf/black)
- [Uncrustify](https://github.com/uncrustify/uncrustify)

## Docs:

- [GTK3](https://docs.gtk.org/gtk3/)
- [ImageMagick](https://imagemagick.org/script/magick-wand.php)
- [Jansson](https://jansson.readthedocs.io/)
- [LBRYNet](https://lbry.tech/api/sdk)
- [MD4C](https://github.com/mity/md4c/wiki)
- [PCRE2](https://pcre.org/current/doc/html/)
- [Pygobject](https://pygobject.readthedocs.io)
- [SQLite3](https://www.sqlite.org/docs.html)

For Python, you could see the [docs](https://docs.python.org/3.11/) and for C extensions the [C API](https://docs.python.org/3.11/c-api/)

## Squashing commits:

When making commits, remember to squash your commits.
You can squash starting with command:

  `git reset --soft HEAD~n`

Where *n* is the number of commits you had already made. After this you should commit your changes e.g. `git commit -a` to add all changed files.

You can then push your commits to remote repository. If you already had pushed commits to your repository, you can issue this command to force update the repository:

  `git push origin +main`

Where '+'-sign forces the repository to take the commits and overwrite any past commits that aren't present in the local version.

**WARNING**: Do not reset more than what you commited, otherwise you might replace commits made by other contributors. Make sure you know exactly how many commits you made before resetting.
