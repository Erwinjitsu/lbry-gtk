#ifndef CHANNEL_H_
#define CHANNEL_H_

#include <jansson.h>

// This function is responsible for getting every single user owned channel
json_t *ChannelGet(char *Server);

#endif
