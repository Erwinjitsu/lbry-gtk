#ifndef FILTER_H_
#define FILTER_H_

#include <jansson.h>

// FallBack strings for missing values
extern char *FilterEmpty, *FilterClaimString, *FilterZero, *FilterAction;

// Keys for error data
extern char *FilterErrMes[], *FilterErrCod[];

// Keys for publication data
extern char *FilterPubTit1[], *FilterPubTit2[], *FilterPubCla[];
extern char *FilterPubTim[], *FilterPubTyp1[], *FilterPubTyp2[];
extern char *FilterPubUrl1[], *FilterPubUrl2[], *FilterPubCha[];
extern char *FilterPubThu[], *FilterPubVal[], *FilterPubNum[];

// These functions are shortcuts to the generic Parse with the specific parser
// They might not be needed
json_t *FilterNotifications(json_t *Data);

json_t *FilterPublications(json_t *Data);

json_t *FilterComments(json_t *Data);

json_t *FilterChannels(json_t *Data);

json_t *FilterBalances(json_t *Data);

json_t *FilterFiles(json_t *Data);

#endif
