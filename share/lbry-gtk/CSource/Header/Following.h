#ifndef FOLLOWING_H_
#define FOLLOWING_H_

#include <jansson.h>

// This function returns if not followed (-1) or the index
// of the followed channel
int FollowingGet(char *Channel);

// This function toggles the following of a channel
int FollowingSet(char *Channel, char *ConfigPath, int Option);

#endif
