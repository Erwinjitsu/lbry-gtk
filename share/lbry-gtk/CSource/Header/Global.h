#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <gtk/gtk.h>
#include <stdbool.h>
#include <jansson.h>

extern GtkBuilder *GlobalBuilder;
extern GtkWindow *GlobalWindow;
extern bool GlobalStarted;
extern json_t *GlobalPreferences, *GlobalSettings;

#endif
