#ifndef ICONS_H_
#define ICONS_H_

#include <gtk/gtk.h>

extern GdkPixbuf *IconsLBCLabel, *IconsLogoBig;

// This function is responsible for creating every icon
void IconsStart(void);

// This function is responsible for freeing every icon
void IconsStop(void);

#endif
