#ifndef LISTUTIL_H_
#define LISTUTIL_H_

#include <gtk/gtk.h>
#include <jansson.h>

// Calculates number of items in list, to fill window
int ListUtilListContent(GtkWidget *MainSpace, GtkWidget *Box);

// Calculates number of items in grid, to fill window
int ListUtilGridContent(GtkWidget *MainSpace, GtkWidget *Box);

// Gets transaction history, fills balance subpart labels
json_t *ListUtilWallet(GtkWidget **WalletSpaceParts, int PageSize, int Page,
	char *Server);

#endif
