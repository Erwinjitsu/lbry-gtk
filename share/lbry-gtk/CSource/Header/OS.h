#ifndef OS_H_
#define OS_H_

#ifdef _WIN32

// Type of OS
#define OSType 0

// Default opener application
#define OSOpener "start"

// Directory separator
#define OSSeparator "\\"
#else
#define OSSeparator "/"
#endif

#ifdef __APPLE__
#define OSType 1
#define OSOpener "open"
#endif

#ifdef __unix__
#define OSType 2
#define OSOpener "xdg-open"
#endif

// This function is responsible for getting the absolute path to binary
// For uniformity
char *OSAbsolutePath(char *Arg0);

#endif
