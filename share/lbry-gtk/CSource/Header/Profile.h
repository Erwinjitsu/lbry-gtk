#ifndef PROFILE_H_
#define PROFILE_H_

#include <Python.h>
#include <gtk/gtk.h>
#include <stdbool.h>

#include "Thumbnail.h"

// Struct for widget data
typedef struct ProfileData {
	// Widgets of Profile used in and outside of this file
	GtkWidget *Image, *Profile, *Channel, *Publications, *ImageActive;

	// Urls for LBRY channel and thumbnail file, name for channel
	char *ChannelUrl, *Title;

	ThumbnailData *Thumbnail;

	// Functions for opening on current/new page
	// TODO: do this with C functions
	PyObject *AddPage, *Lister, *GetPublicationThread;
} ProfileData;

// This function is responsible for creating the Profile widget
ProfileData *ProfileCreate(char *ChannelUrl, char *Thumbnail, float ChannelLBC,
	float PostLBC, int UploadNumber, unsigned int Stamp,
	int Size, bool ProfileImage,
	PyObject *GetPublicationThread, PyObject *AddPage,
	PyObject *Lister, char *Title, bool Circled,
	int MaxWidth);

#endif
