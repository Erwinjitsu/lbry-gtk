#ifndef REQUEST_H_
#define REQUEST_H_

#include <stdbool.h>
#include <jansson.h>

// This function gets the json response from url
json_t *RequestMake(char *Server, char *JsonString, char *Parameters);

// This function checks the string is Json and sends it
json_t *RequestJson(char *Server, char *JsonString);

// This function is responsible for downloading a file from Url to Path
bool RequestFile(char *Url, char *Path);

#endif
