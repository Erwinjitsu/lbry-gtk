#ifndef SUPPORT_H_
#define SUPPORT_H_

#include <stdbool.h>
#include <jansson.h>

// This function sends a tip or a boost to a claim
json_t *SupportCreate(char *ClaimId, double Amount, bool Tip, char *ChannelName,
	char *ChannelId, char *Server);

#endif
