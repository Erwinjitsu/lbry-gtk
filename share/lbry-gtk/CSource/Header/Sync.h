#ifndef SYNC_H_
#define SYNC_H_

#include <jansson.h>

// This function will unsync the account
int SyncDisconnect(json_t *AccountIDs, char *Server);

// Syncs the account with lbrynet
json_t *SyncConnect(char *Email, char *Password, char *Server);

#endif
