/******************************************************************************\
* LBRY-GTK                                                                     *
* Copyright (C) 2021-2023 MorsMortium and Other Contributors                   *
* This program is free software: you can redistribute it and/or modify         *
* it under the terms of the GNU General Public License as published by         *
* the Free Software Foundation, either version 3 of the License, or            *
* (at your option) any later version.                                          *
*                                                                              *
* This program is distributed in the hope that it will be useful,              *
* but WITHOUT ANY WARRANTY; without even the implied warranty of               *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                *
* GNU General Public License for more details.                                 *
*                                                                              *
* You should have received a copy of the GNU General Public License            *
* along with this program.  If not, see <http://www.gnu.org/licenses/>.        *
\******************************************************************************/

// This file is responsible for operating system specific parts

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "Header/OS.h"

char *OSAbsolutePath(char *Arg0) {
	// This function is responsible for getting the absolute path to binary
	// For uniformity
	char *Absolute = malloc(2);
	sprintf(Absolute, " ");

#ifdef _WIN32

	// Windows arg 0 contains absolute path, make it malloced for uniformity
	Absolute = realloc(Absolute, strlen(Arg0) + 1);
	sprintf(Absolute, "%s", Arg0);
#endif

#ifdef __unix__

	// Get proc file for executable
	FILE *File = fopen("/proc/self/maps", "r");

	// Get length of string reaching first line with lbry-gtk binary
	int Length = 1;

	// Letters in file for binary
	char Name[9] = {'l', 'b', 'r', 'y', '-', 'g', 't', 'k', '\n'};
	int Index = 0;

	do {
		// Read single character
		char Character = fgetc(File);

		if (Character == Name[Index]) {
			// If character is correct
			if (Index == 8) {
				// If character is last, exit
				break;
			} else  {
				// If not, go to next character
				++Index;
			}
		} else {
			// If not, start over
			Index = 0;
		}
		++Length;
	} while (true);

	// Get string ending with binary, close file
	char Line[Length];
	rewind(File);
	for (int i = 0; i < Length - 1; ++i) {
		Line[i] = fgetc(File);
	}
	Line[Length - 1] = '\0';
	fclose(File);

	// Remove everything before last space (including space)
	char *Space = strrchr(Line, ' ');
	memmove(Line, Space + 1, strlen(Space));

	// Make path malloced
	Absolute = realloc(Absolute, strlen(Line) + 1);
	sprintf(Absolute, "%s", Line);
#endif

	return Absolute;
}
