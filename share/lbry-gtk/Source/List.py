################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, queue, copy, time

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, GdkPixbuf

from Source.PyGObjectCAPI import PyGObjectCAPI
from CRewrite import Box, Content, DateTime, File, Image, ListUtil
from CRewrite import NotificationDB, Order, Popup, Preferences, Replace, Tag

ClaimTypes = {"stream": 1, "channel": 2, "repost": 3, "collection": 4}
ListTypes = ["Content", "Notification", "Wallet", "File"]

CAPI = PyGObjectCAPI()


class List:
    ListPage, Loaded, Started, RowCache = 0, 1, False, []

    def __init__(self, *args):
        (
            self.NoContentNotice,
            self.Publicationer,
            WalletSpaceParts,
            self.MainSpace,
            self.Stater,
            self.Grid,
            self.Spaces,
            self.Text,
            self.Orderer,
            self.Channel,
            self.ClaimType,
            self.TaggersDict,
            self.DateTimer,
            self.Inequality,
            self.Advanced,
            self.DateType,
            self.Title,
            self.AddPage,
        ) = args
        self.Boxer = ""
        self.GetPublicationThread = self.Publicationer.GetPublicationThread

        self.FlowBoxPointer = CAPI.AsVoidPointer(self.Grid.__gpointer__)

        self.ListFunctions = {
            "Search": {"Function": Content.Search, "Data": None},
            "Wallet": {"Function": ListUtil.Wallet, "Data": WalletSpaceParts},
            "File": {"Function": File.List, "Data": None},
            "Inbox": {"Function": NotificationDB.List, "Data": ""},
        }
        self.Threads = []

    def UpdateMainSpace(self, LBRYSettings, Increase=0):
        self.Loaded += Increase
        for Thread in self.Threads:
            Thread[1].get()
        ListFunction = self.ListFunctions[self.ListButton]
        self.ListThread(
            LBRYSettings, ListFunction["Function"], ListFunction["Data"]
        )

    def Empty(self, Queue):
        self.Grid.forall(Gtk.Widget.destroy)
        Queue.put(True)

    def Exit(self, ThreadQueue, UpdateMainSpaceQueue):
        self.Threads.remove([ThreadQueue, UpdateMainSpaceQueue])
        UpdateMainSpaceQueue.put(True)
        self.Started = True

    def ListThread(self, LBRYSettings, Function, FunctionData):
        ThreadQueue = queue.Queue()
        UpdateMainSpaceQueue = queue.Queue()
        self.Threads.append([ThreadQueue, UpdateMainSpaceQueue])
        LBRYGTKSettings = LBRYSettings["Preferences"]
        Server = LBRYGTKSettings["Session"]["Server"]
        while self.ListPage < self.Loaded:
            SingleMessage = True
            while True:
                # Data request
                if FunctionData != None:
                    Data = Function(
                        FunctionData,
                        max(1, int(LBRYGTKSettings["ContentPerLoading"])),
                        self.ListPage + 1,
                        Server,
                    )
                else:
                    Data = Function(
                        max(1, int(LBRYGTKSettings["ContentPerLoading"])),
                        self.ListPage + 1,
                        Server,
                    )
                Data = json.loads(Data)
                # Returned number tells the page number change (lbrynet)
                PlacedQueue = queue.Queue()
                GLib.idle_add(
                    self.PlaceData,
                    LBRYGTKSettings,
                    ThreadQueue,
                    Data,
                    PlacedQueue,
                )
                PlacedReturn = PlacedQueue.get()

                # Try to reget data, if an error occured
                if PlacedReturn != -1:
                    break
                elif SingleMessage:
                    Popup.Message(
                        "LBRYNet is experiencing a problem, the client is retrying to load the data.",
                    )
                    Popup.Error(PlacedQueue.get())
                    SingleMessage = False
                try:
                    ThreadQueue.get(block=False)
                    break
                except:
                    pass

            if PlacedReturn == 1:
                self.ListPage += 1
            # If placing data wasn't succesful - break execution
            if PlacedReturn != 1:
                break
        self.Exit(ThreadQueue, UpdateMainSpaceQueue)

    def PlaceData(self, LBRYGTKSettings, ThreadQueue, Data, Queue):
        Space = self.Spaces[self.List]
        if len(Data) == 0:
            if self.ListPage == 0 and not self.NoContentNotice.get_parent():
                Space.add(self.NoContentNotice)
                self.MainSpace.show_all()
            Queue.put(0)
            return
        if "error" in Data:
            Queue.put(-1)
            Queue.put(Data["error"])
            return
        if self.ListPage == 0:
            Space.add(self.Grid)
        Boxer = ""
        # Data is placed to grid/list here
        for Row in Data:
            try:
                ThreadQueue.get(block=False)
                Queue.put(-2)
                return
            except:
                pass

            # Checks if already exists on the page
            if (
                Row["Claim"] in self.RowCache
                and self.List != "Wallet"
                and self.ListButton != "Inbox"
            ):
                continue
            else:
                self.RowCache.append(Row["Claim"])

            self.Boxer = Box.Create(
                self.ListDisplay != 0,
                int(LBRYGTKSettings["ThumbnailWidth"]),
                Row,
                self.GetPublicationThread,
                int(LBRYGTKSettings["GridTitleRows"]),
                int(LBRYGTKSettings["BoxPadding"]),
                ListTypes.index(self.List),
                self.AddPage,
                int(LBRYGTKSettings["ThumbnailRounding"]),
                int(LBRYGTKSettings["PublicationProfileCircling"]),
                self,
                int(LBRYGTKSettings["ListChannelWidth"]),
                self.FlowBoxPointer,
                int(LBRYGTKSettings["PublicationProfile"]),
                int(LBRYGTKSettings["PublicationProfileSize"]),
            )
            self.MainSpace.show_all()
        self.GetContent(LBRYGTKSettings)
        Queue.put(1)

    def GetContent(self, LBRYGTKSettings):
        ContentNumber = 1
        if self.Boxer != "":
            ContentFunction = ListUtil.GridContent
            if self.ListDisplay == 0:
                ContentFunction = ListUtil.ListContent
            ContentNumber = ContentFunction(
                CAPI.AsVoidPointer(self.MainSpace.__gpointer__),
                self.Boxer["Box"],
            )
            ContentNumber = (
                ContentNumber // int(LBRYGTKSettings["ContentPerLoading"]) + 1
            )
        if self.Loaded < ContentNumber:
            self.Loaded = ContentNumber

    def GenericLoaded(self, LBRYSettings):
        self.GetContent(LBRYSettings["Preferences"])
        if self.ListPage < self.Loaded:
            threading.Thread(
                None, self.UpdateMainSpace, None, [LBRYSettings], daemon=True
            ).start()

    def ButtonMakeThread(self, *Args):
        threading.Thread(
            None, self.ButtonThread, None, Args, daemon=True
        ).start()

    def ButtonThread(self, ButtonName, ListName, Title, Data={}, State=False):
        Image.IncreaseCount()
        self.Publicationer.Commenter.KillAll()
        EmptyQueue = queue.Queue()
        GLib.idle_add(self.Empty, EmptyQueue)
        EmptyQueue.get()

        self.RowCache.clear()
        self.Started, self.Loaded = False, 0
        for Thread in self.Threads:
            Thread[0].put(True)
        while len(self.Threads) != 0:
            time.sleep(0.01)
        LBRYSettings = json.loads(Preferences.Get())
        if "error" in LBRYSettings:
            Popup.Error(LBRYSettings["error"])
            return
        LBRYGTKSettings = LBRYSettings["Preferences"]
        self.ListDisplay = LBRYGTKSettings["ListDisplay"]
        Args = [ButtonName, ListName, Title, Data]
        if not State:
            self.Stater.Save(self.ButtonThread, Args, Title)
        GLib.idle_add(self.ButtonUpdate, *Args, LBRYSettings)

    def ButtonUpdate(self, ButtonName, ListName, Title, Data, LBRYSettings):
        LBRYGTKSettings = LBRYSettings["Preferences"]

        self.Grid.set_max_children_per_line(self.ListDisplay * 999 + 1)

        # Initialization
        self.Advanced.set_expanded(True)
        self.Advanced.activate()
        self.Advanced.set_expanded(False)
        Order.Set(self.Orderer["Pointer"], LBRYGTKSettings["Order"])
        self.Text.set_text("")
        self.Channel.set_text("")
        self.ClaimType.set_active(0)
        self.Inequality.set_active(0)
        self.DateType.set_active(0)
        for Index in self.TaggersDict:
            # Find the pointer
            Pointer = 0
            try:
                Pointer = self.TaggersDict[Index][1]["TaggerPointer"]
            except:
                Pointer = self.TaggersDict[Index][1]["Pointer"]
            Tag.RemoveAll(Pointer)
        # Check for calendar stuff
        Time = "-1"
        if "timestamp" in Data:
            Time = Data["timestamp"]
            if Time != "-1":
                Equality = [">=", "<=", "=", ">", "<"]
                Equal = 2
                for Mark in Equality:
                    if Time.startswith(Mark):
                        Equal = 4 - Equality.index(Mark)
                        Time = Time[len(Mark) :]
                        break
                self.DateType.set_active(2)
                self.Inequality.set_active(Equal)

        # All DateTime stuff gathered here
        DatePointer = self.DateTimer["Pointer"]
        CAPI.ToObject(self.DateTimer["DateUse"]).set_active(Time != "-1")
        if Time != "-1":
            DateTime.SetTime(DatePointer, int(Time))
        else:
            DateTime.OnDateResetButtonPressEvent(DatePointer)

        NotConverts = {"not_tags": "TagsNot", "not_channel_ids": "ChannelsNot"}

        # Settings found from Data placed
        ShowMature = LBRYGTKSettings["ShowMature"]
        if ListName == "Content":
            if "order_by" in Data:
                if Data["order_by"] == []:
                    Order.Set(self.Orderer["Pointer"], "")
                else:
                    Order.Set(self.Orderer["Pointer"], Data["order_by"][0])
            else:
                OrderBy = Order.Get(self.Orderer["Pointer"])
                if OrderBy:
                    Data["order_by"] = [OrderBy]
            for NotConvert in NotConverts:
                if not NotConvert in Data:
                    Data[NotConvert] = []
                if LBRYGTKSettings[NotConverts[NotConvert]] != []:
                    Data[NotConvert].extend(
                        copy.deepcopy(LBRYGTKSettings[NotConverts[NotConvert]])
                    )
            if not ShowMature:
                Data["not_tags"].append("mature")
                LBRYGTKSettings["TagsNot"].append("mature")

        if "text" in Data:
            self.Text.set_text(Data["text"])
        if "channel" in Data:
            self.Channel.set_text(Data["channel"])
        if "claim_type" in Data:
            self.ClaimType.set_active(ClaimTypes[Data["claim_type"]])

        for Index in self.TaggersDict:
            if Index not in Data:
                continue
            Tags = []
            if Index in NotConverts:
                # Add any tags to the not tags part of tags
                for GotTag in Data[Index]:
                    if not GotTag in LBRYGTKSettings[NotConverts[Index]]:
                        Tags.append(GotTag)
            else:
                # Normal tags
                Tags = Data[Index]

            # Find the pointer
            Pointer = 0
            try:
                Pointer = self.TaggersDict[Index][1]["TaggerPointer"]
            except:
                Pointer = self.TaggersDict[Index][1]["Pointer"]

            # Attempt to append to the tags
            Tag.Append(Tags, Pointer)

        self.ListButton = ButtonName
        self.List = ListName
        self.Title.set_text(Title)
        for Child in [self.Grid, self.NoContentNotice]:
            Parent = Child.get_parent()
            if Parent:
                Parent.remove(Child)
        Replace.Replace(self.Replacer, self.List + "List")
        self.ListPage = 0
        self.Loaded = 1
        if ButtonName == "Search":
            self.ListFunctions["Search"]["Data"] = json.dumps(Data)
        threading.Thread(
            None, self.UpdateMainSpace, None, [LBRYSettings], daemon=True
        ).start()
