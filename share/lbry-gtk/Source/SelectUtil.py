################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2023 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gdk, Gtk

from CRewrite import DateTime, Language, Markdown, Tag, Thumbnail

Types = [Gdk.BUTTON_PRIMARY, Gdk.BUTTON_MIDDLE]


def Event(Widget, Button):
    Event = Gdk.Event.new(Gdk.EventType.BUTTON_PRESS)
    Event.button = Button
    Event.window = Widget.get_window()
    Widget.event(Event)


def SendEvent(Function, Widget, Button):
    Event(Widget, Types[Button])
    if Function != "":
        Function()


def Functions(AllFunctions):
    AllFunctions = [*AllFunctions]
    for Function in AllFunctions:
        Function()


def FunctionsLambda(*AllFunctions):
    return lambda Argument="": Functions(AllFunctions)


def EventLambda(MenuItem, Function):
    return lambda Button: SendEvent(Function, MenuItem, Button)


def FunctionLambda(Object, Function):
    return lambda Button="": Function(Object, Button)


def FunctionNoButtonLambda(Object, Function):
    return lambda Button="": Function(Object)


OrderSorting = ["UseOrdering", "UseOrderingActive", "OrderBy", "GtkLabel"]
OrderSorting.extend(["OrderByActive", "Direction", "DirectionActive"])


def OrderSorter(Widget):
    return OrderSorting.index(Widget.get_name())


def PrepareNew(Items, NewWidgets, Taggers=[], Languagers=[], Daters=[]):
    Prepared = PrepareWidgets(NewWidgets, Taggers, Languagers, Daters)
    for Index in range(len(Items)):
        Items[Index].extend(Prepared[Index])


def LevelLambda(Widget, Value):
    return lambda: Widget.set_value(Value)


def PrepareWidgets(AllWidgets, Taggers=[], Languagers=[], Daters=[]):
    Widgets, Exceptions, Enters, Leaves, Activates = [], [], [], [], []
    Return = [Widgets, Exceptions, Enters, Leaves, Activates]
    for Widget in AllWidgets:
        if callable(Widget):
            Activates.append(Widget)
            continue
        if isinstance(Widget, dict) and "Frame" in Widget:
            Activates.append(
                FunctionNoButtonLambda(Widget["Pointer"], Thumbnail.PressCheck)
            )
            continue
        if (
            isinstance(Widget, Gtk.SpinButton)
            or isinstance(Widget, Gtk.CheckButton)
            or isinstance(Widget, Gtk.FileChooserButton)
            or isinstance(Widget, Gtk.Entry)
            or isinstance(Widget, Gtk.ComboBoxText)
            or isinstance(Widget, Gtk.ComboBox)
            or isinstance(Widget, Gtk.TextView)
            or Widget.get_name() == "Document"
            or Widget.get_name().startswith("Date")
        ):
            Widgets.append(Widget)
            Exceptions.append(Widget)
        if (
            isinstance(Widget, Gtk.SpinButton)
            or isinstance(Widget, Gtk.Entry)
            or isinstance(Widget, Gtk.TextView)
        ):
            Enters.append(Widget.grab_focus)
            Leaves.append("")
            Activates.append(Widget.grab_focus)
        elif isinstance(Widget, Gtk.CheckButton):
            Activates.append(Widget.activate)
        elif isinstance(Widget, Gtk.LevelBar):
            Enters.append(LevelLambda(Widget, 1))
            Leaves.append(LevelLambda(Widget, 0))
        elif isinstance(Widget, Gtk.FileChooserButton):
            if Widget.get_action() == Gtk.FileChooserAction.SELECT_FOLDER:
                Activates.append(Widget.get_children()[1].popup)
            else:
                Activates.append(Widget.get_children()[0].activate)
        elif isinstance(Widget, Gtk.ComboBoxText):
            Activates.append(Widget.popup)
        elif isinstance(Widget, Gtk.ComboBox):
            Activates.append(Widget.popup)
            Enters.append(Widget.get_children()[0].grab_focus)
            Leaves.append("")
        elif isinstance(Widget, Gtk.EventBox):
            Widgets.append(Widget)
            Exceptions.append(Widget)
            Activates.append(EventLambda(Widget, ""))
        elif (
            isinstance(Widget, Gtk.ToggleButton)
            or Widget.get_name() == "FakeItem"
        ):
            Widgets.append(Widget)
        elif Widget.get_name() == "OrderBox":
            NewWidgets = Widget.get_children()[0].get_children()
            NewWidgets.sort(key=OrderSorter)
            PrepareNew(Return, NewWidgets)
        elif Widget.get_name() == "KeyBind":
            Boxes = Widget.get_children()
            NewWidgets = []
            for Box in Boxes:
                try:
                    NewWidgets.extend(Box.get_children())
                except:
                    NewWidgets.append(Box)
            PrepareNew(Return, NewWidgets)
        elif Widget.get_name() == "CommentChannelBox":
            PrepareNew(Return, Widget.get_children()[0].get_children())
        elif Widget.get_name() == "Auth":
            PrepareNew(Return, Widget.get_children())
        elif isinstance(Widget, Gtk.Action):
            Activates.append(Widget.activate)
        elif Widget.get_name().startswith("Tag"):
            Index = int(Widget.get_name().split("-")[1])
            PrepareNew(Return, [Widget.get_children()[0].get_children()[0]])
            Activates[-1] = FunctionLambda(
                Taggers[Index]["Pointer"], Tag.KeybindHelper
            )
        elif Widget.get_name().startswith("LanguageBox"):
            Index = int(Widget.get_name().split("-")[1])
            BoxChildren = Widget.get_children()[0].get_children()
            PrepareNew(Return, [BoxChildren[0]])
            Enters.append(BoxChildren[0].get_children()[0].grab_focus)
            Leaves.append("")
            Activates[-1] = FunctionLambda(
                Languagers[Index]["Pointer"], Language.KeybindHelper
            )
        elif Widget.get_name().startswith("Date"):
            Index = int(Widget.get_name().split("-")[1])
            Activates.append(
                FunctionLambda(Daters[Index]["Pointer"], DateTime.DateChange)
            )
    return [Widgets, Exceptions, Enters, Leaves, Activates]
